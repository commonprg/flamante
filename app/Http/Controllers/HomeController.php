<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class HomeController extends Controller
{
    
    private $view = 'theme.';

    public function Home()
    {
        return view($this->view.'index');
    }

}
